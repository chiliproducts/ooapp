package com.example.sali.myapplication;

import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.os.Build;
import android.util.AttributeSet;
import android.view.View;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Sali on 2016.04.04..
 */
public class Graph extends View {

    public final static int MAX_VALUES=50;
    private List<Float> values;

    private Path line;

    private Paint paint;
    public Graph(Context context) {
        super(context);
        init();
    }

    public Graph(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public Graph(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public Graph(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init();
    }
    private void init()
    {
        this.line= new Path();
        this.values=new ArrayList<Float>(MAX_VALUES);
        this.paint= new Paint();
        this.paint.setColor(Color.BLUE);
        this.paint.setStyle(Paint.Style.STROKE);
        this.paint.setStrokeWidth(7f);
        this.paint.setStrokeJoin(Paint.Join.ROUND);
        this.paint.setAntiAlias(true);

        for(int i=0;i< MAX_VALUES;i++)
            this.values.add(0f);


    }

    public void addValue(float value)
    {

        this.values.remove(0);
        this.values.add(value);

        invalidate();



    }
    @Override
    protected  void onDraw(Canvas canvas)
    {
        super.onDraw(canvas);

        int width =getWidth();
        int height=getHeight();

        int height_offset = height >>1;//bitenkénti osztás,osztás 2vel

        float width_step = width/ values.size();
        float height_step= width_step;

        this.line.reset(); //resetelünk nem edig újragyártunk

        //2 hasznos metódus. moveto nem rajzol,lineto rajzol

        this.line.moveTo(0,values.get(0)*height_step+height_offset);

        for(int i=1;i<values.size();i++)
        {
            this.line.lineTo( i*width_step,values.get(i)*height_step+height_step);




        }
        canvas.drawPath(this.line,this.paint);


    }
}
