package hu.uniobuda.nik.chili.ooapp;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatTextView;
import android.util.Log;
import android.view.View;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class FreshMenuActivity extends AppCompatActivity {


    String url = "http://marta-vas.hu/get_latest_data.php?sensor=temperature";
    String url2 = "http://marta-vas.hu/get_latest_data.php?sensor=humidity";
    String url3 = "http://marta-vas.hu/get_latest_data.php?sensor=water";
    String url4 = "http://marta-vas.hu/get_latest_data.php?sensor=window";
    String url5 = "http://marta-vas.hu/get_latest_data.php?sensor=rain";
    String url6 = "http://marta-vas.hu/get_latest_data.php?sensor=co";

    AppCompatButton refreshButton;
    AppCompatTextView temperature;
    AppCompatTextView humidity;
    AppCompatTextView windows;
    AppCompatTextView water;
    AppCompatTextView rain;
    AppCompatTextView co;

    RequestQueue queue;
    RequestQueue queue1;
    RequestQueue queue2;
    RequestQueue queue3;
    RequestQueue queue4;
    RequestQueue queue5;
    RequestQueue queue6;

    ProgressDialog pd;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fresh_menu);
        addListenerOnButton();
        LoadData();
    }

    public void addListenerOnButton() {

        AppCompatButton btnRefresh = (AppCompatButton) findViewById(R.id.btnRefresh);


        btnRefresh.setOnClickListener(new View.OnClickListener() {


            @Override
            public void onClick(View v) {

                LoadData();
            }
        });
    }

    public void LoadData() {

        AppCompatButton refreshButton = (AppCompatButton) findViewById(R.id.btnRefresh);
        temperature = (AppCompatTextView) findViewById(R.id.txtInternalCelsius);
        humidity = (AppCompatTextView) findViewById(R.id.textHumidityCtd);
        windows = (AppCompatTextView) findViewById(R.id.textWindowsState);
        water = (AppCompatTextView) findViewById(R.id.txtWaterConsumptionValue);
        rain = (AppCompatTextView) findViewById(R.id.txtRainState);
        co = (AppCompatTextView) findViewById(R.id.txtCoConcentrationState);

        String url = "http://marta-vas.hu/get_latest_datas.php";
        queue = Volley.newRequestQueue(FreshMenuActivity.this);

        JsonObjectRequest stringRequest = new JsonObjectRequest(Request.Method.GET, url, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.i("response", "Response is: " + response.toString());
                        try {
                            if (response.getInt("success") == 1) {
                                List<Measurement> measurements = new ArrayList<Measurement>();
                                JSONArray measurementsJSON = response.getJSONArray("measurements");

                                for (int i = 0; i < measurementsJSON.length(); i++) {
                                    JSONObject obj = measurementsJSON.getJSONObject(i).getJSONObject("measurement");
                                    measurements.add(new Measurement(obj));
                                }
                                for (Measurement m : measurements) {
                                    Log.i("asd", m.getSensor());
                                    if (m.getSensor().equals("temperature")) {
                                        temperature.setText(m.getValue() + " °C");
                                    }
                                    if (m.getSensor().equals("humidity")) {
                                        humidity.setText(m.getValue() + " %");
                                    }
                                    if (m.getSensor().equals("window")) {
                                        if (m.getValue() == 0)
                                            windows.setText(R.string.isOpened);
                                        else windows.setText(R.string.isClosed);
                                    }
                                    if (m.getSensor().equals("water")) {
                                        water.setText(m.getValue() + " m3");
                                    }
                                    if (m.getSensor().equals("water")) {
                                        if (m.getValue() == 0)
                                            rain.setText(R.string.notRaining);
                                        else rain.setText(R.string.isRaining);
                                    }
                                    if (m.getSensor().equals("co")) {
                                        co.setText(m.getValue() + " ppm");
                                    }
                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.i("error", "error");
            }
        });

        queue.add(stringRequest);

    }

    public void LoadingData()

    {
        AppCompatButton refreshButton = (AppCompatButton) findViewById(R.id.btnRefresh);
        temperature = (AppCompatTextView) findViewById(R.id.txtInternalCelsius);
        humidity = (AppCompatTextView) findViewById(R.id.textHumidityCtd);
        windows = (AppCompatTextView) findViewById(R.id.textWindowsState);
        water = (AppCompatTextView) findViewById(R.id.txtWaterConsumptionValue);
        rain = (AppCompatTextView) findViewById(R.id.txtRainState);
        co = (AppCompatTextView) findViewById(R.id.txtCoConcentrationState);

        queue1 = Volley.newRequestQueue(FreshMenuActivity.this);
        queue2 = Volley.newRequestQueue(FreshMenuActivity.this);
        queue3 = Volley.newRequestQueue(FreshMenuActivity.this);
        queue4 = Volley.newRequestQueue(FreshMenuActivity.this);
        queue5 = Volley.newRequestQueue(FreshMenuActivity.this);
        queue6 = Volley.newRequestQueue(FreshMenuActivity.this);

        JsonObjectRequest stringRequest = new JsonObjectRequest(Request.Method.GET, url, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.i("response", "Response is: " + response.toString());
                        try {
                            if (response.getInt("success") == 1) {
                                Measurement measurement = new Measurement(response.getJSONObject("measurement"));
                                temperature.setText(measurement.getValue() + " °C");
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.i("error", "error");
            }
        });

        queue1.add(stringRequest);

        JsonObjectRequest stringRequest2 = new JsonObjectRequest(Request.Method.GET, url2, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.i("response", "Response is: " + response.toString());
                        try {
                            if (response.getInt("success") == 1) {
                                Measurement measurement = new Measurement(response.getJSONObject("measurement"));
                                humidity.setText(measurement.getValue() + " °Ctd");
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.i("error", "error");
            }
        });

        queue2.add(stringRequest2);

        JsonObjectRequest stringRequest3 = new JsonObjectRequest(Request.Method.GET, url4, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.i("response", "Response is: " + response.toString());
                        try {
                            if (response.getInt("success") == 1) {
                                Measurement measurement = new Measurement(response.getJSONObject("measurement"));
                                if (measurement.getValue() == 0)
                                    windows.setText("CLOSED");
                                else windows.setText("OPEN");
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.i("error", "error");
            }
        });

        queue3.add(stringRequest3);

        JsonObjectRequest stringRequest4 = new JsonObjectRequest(Request.Method.GET, url3, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.i("response", "Response is: " + response.toString());
                        try {
                            if (response.getInt("success") == 1) {
                                Measurement measurement = new Measurement(response.getJSONObject("measurement"));

                                water.setText(measurement.getValue() + " m^3");

                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.i("error", "error");
            }
        });

        queue4.add(stringRequest4);

        JsonObjectRequest stringRequest5 = new JsonObjectRequest(Request.Method.GET, url5, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.i("response", "Response is: " + response.toString());
                        try {
                            if (response.getInt("success") == 1) {
                                Measurement measurement = new Measurement(response.getJSONObject("measurement"));
                                if (measurement.getValue() == 0)
                                    rain.setText("NO");
                                else rain.setText("YES");
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.i("error", "error");
            }
        });

        queue5.add(stringRequest5);

        JsonObjectRequest stringRequest6 = new JsonObjectRequest(Request.Method.GET, url6, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.i("response", "Response is: " + response.toString());
                        try {
                            if (response.getInt("success") == 1) {
                                Measurement measurement = new Measurement(response.getJSONObject("measurement"));

                                co.setText(measurement.getValue() + " ppm");

                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.i("error", "error");
            }
        });

        queue6.add(stringRequest6);
    }

}
