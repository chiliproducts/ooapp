package hu.uniobuda.nik.chili.ooapp;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatSpinner;
import android.support.v7.widget.AppCompatTextView;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Sali on 2016.03.19..
 */

public class SumChartsMenuActivity extends AppCompatActivity {

    private static LineChart lineChart;
    private static LineDataSet dataSet;
    private static List<String> labels;
    private LineData data;
    private RequestQueue queue;


    private AppCompatButton btnGo;
    private AppCompatSpinner spinner;


    private List<Measurement> measurements;
    private String selectedSensor;

    private AppCompatTextView errorMessage;


    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sumcharts_menu);

        lineChart = (LineChart) findViewById(R.id.chart);
        lineChart.setVisibility(View.INVISIBLE);
        measurements = new ArrayList<>();

        btnGo = (AppCompatButton) findViewById(R.id.btn_go);
        spinner = (AppCompatSpinner) findViewById(R.id.spinner);
        ArrayAdapter dataAdapter = ArrayAdapter.createFromResource(this,
                R.array.planets_array, R.layout.spinner_item);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(dataAdapter);
        selectedSensor = spinner.getSelectedItem().toString().toLowerCase();
        switch (selectedSensor) {
            case "hőmérséklet":
                selectedSensor = "temperature";
        }
        errorMessage = (AppCompatTextView) findViewById(R.id.errorMessage);
        errorMessage.setVisibility(View.INVISIBLE);

        btnGo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectedSensor = spinner.getSelectedItem().toString().toLowerCase();
                switch (selectedSensor) {
                    case "hőmérséklet":
                        selectedSensor = "temperature";
                        break;
                    case "páratartalom":
                        selectedSensor = "humidity";
                        break;
                    case "co koncentráció":
                        selectedSensor = "CO";
                        break;
                    case "vízfogyasztás":
                        selectedSensor = "water";
                        break;
                    case "eső":
                        selectedSensor = "rain";
                        break;
                    case "ablak":
                        selectedSensor = "window";
                        break;
                }
                LoadData();
            }
        });

    }

    public void LoadData()
    {

        String url = "http://marta-vas.hu/get_data_by_sensor.php?sensor="+ selectedSensor+"&from=2016-04-08&to=2016-04-09";

        queue = Volley.newRequestQueue(SumChartsMenuActivity.this);

        JsonObjectRequest stringRequest = new JsonObjectRequest(Request.Method.GET, url,null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.i("response", "Response is: " + response.toString());
                        try {
                            if (response.getInt("success") == 1) {

                                measurements.clear();
                                JSONArray measurementsJSON = response.getJSONArray("measurements");

                                for (int i = 0; i < measurementsJSON.length(); i++) {
                                    JSONObject obj = measurementsJSON.getJSONObject(i).getJSONObject("measurement");
                                    measurements.add(new Measurement(obj));
                                }
                                ArrayList<Entry> entries = new ArrayList<>();


                                for (int i = 0; i < measurements.size(); i++) {
                                    entries.add(new Entry(measurements.get(i).getValue(), i));
                                }

                                dataSet = new LineDataSet(entries, spinner.getSelectedItem().toString());
                                dataSet.setColor(Color.BLACK);

                                labels = new ArrayList<String>();

                                for (Measurement m : measurements) {
                                    labels.add(m.getMeasured());
                                }

                                data = new LineData(labels, dataSet);

                                lineChart.setVisibility(View.VISIBLE);
                                lineChart.setDescription("");
                                lineChart.setData(data);
                                lineChart.invalidate();
                            } else {
                                lineChart.setVisibility(View.INVISIBLE);
                                errorMessage.setVisibility(View.VISIBLE);
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.i("error", "error");
                lineChart.setVisibility(View.INVISIBLE);
                errorMessage.setVisibility(View.VISIBLE);
            }
        });



        queue.add(stringRequest);





    }

}