package hu.uniobuda.nik.chili.ooapp;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatSpinner;
import android.support.v7.widget.AppCompatTextView;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class DailyChartActivity extends AppCompatActivity {

    private static LineChart lineChart;
    private static LineDataSet dataSet;
    private static List<String> labels;
    private LineData data;
    private RequestQueue queue;
    private TextView tvDisplayDate;
    private DatePicker dpResult;
    private AppCompatButton btnChangeDate;
    private AppCompatButton btnGo;
    private AppCompatSpinner spinner;
    private int year;
    private int month;
    private int day;
    private int newYear;
    private int newMonth;
    private int newDay;
    private int nextDay;
    static final int DATE_DIALOG_ID = 999;
    private List<Measurement> measurements;
    private String selectedSensor;
    private String fromDate;
    private String toDate;
    private AppCompatTextView errorMessage;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_daily_chart);

        lineChart = (LineChart) findViewById(R.id.chart);
        lineChart.setVisibility(View.INVISIBLE);
        measurements = new ArrayList<>();
        setCurrentDateOnView();
        addListenerOnButton();
        btnGo = (AppCompatButton) findViewById(R.id.btn_go);

        spinner = (AppCompatSpinner) findViewById(R.id.spinner);
        ArrayAdapter dataAdapter = ArrayAdapter.createFromResource(this,
                R.array.planets_array, R.layout.spinner_item);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(dataAdapter);
        selectedSensor = spinner.getSelectedItem().toString().toLowerCase();
        switch (selectedSensor) {
            case "hőmérséklet":
                selectedSensor = "temperature";
        }
        errorMessage = (AppCompatTextView) findViewById(R.id.errorMessage);
        errorMessage.setVisibility(View.INVISIBLE);

        btnGo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectedSensor = spinner.getSelectedItem().toString().toLowerCase();
                switch (selectedSensor) {
                    case "hőmérséklet":
                        selectedSensor = "temperature";
                        break;
                    case "páratartalom":
                        selectedSensor = "humidity";
                        break;
                    case "co koncentráció":
                        selectedSensor = "CO";
                        break;
                    case "vízfogyasztás":
                        selectedSensor = "water";
                        break;
                    case "eső":
                        selectedSensor = "rain";
                        break;
                    case "ablak":
                        selectedSensor = "window";
                        break;
                }
                LoadData();
            }
        });

    }

    public void LoadData() {
        String url = "http://marta-vas.hu/get_data_by_measured.php?sensor=" + selectedSensor + "&from=" + fromDate + "&to=" + toDate;
        queue = Volley.newRequestQueue(DailyChartActivity.this);
        JsonObjectRequest stringRequest = new JsonObjectRequest(Request.Method.GET, url, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.i("response", "Response is: " + response.toString());
                        try {
                            if (response.getInt("success") == 1) {
                                errorMessage.setVisibility(View.INVISIBLE);
                                measurements.clear();
                                JSONArray measurementsJSON = response.getJSONArray("measurements");

                                for (int i = 0; i < measurementsJSON.length(); i++) {
                                    JSONObject obj = measurementsJSON.getJSONObject(i).getJSONObject("measurement");
                                    measurements.add(new Measurement(obj));
                                }
                                // creating list of entry
                                ArrayList<Entry> entries = new ArrayList<>();


                                for (int i = 0; i < measurements.size(); i++) {
                                    entries.add(new Entry(measurements.get(i).getValue(), i));
                                }

                                dataSet = new LineDataSet(entries, spinner.getSelectedItem().toString());
                                dataSet.setColor(Color.BLACK);

                                labels = new ArrayList<String>();

                                for (Measurement m : measurements) {
                                    labels.add(m.getMeasured());
                                }

                                data = new LineData(labels, dataSet);

                                lineChart.setVisibility(View.VISIBLE);
                                lineChart.setDescription("");
                                lineChart.setData(data);
                                lineChart.invalidate();
                            } else {
                                lineChart.setVisibility(View.INVISIBLE);
                                errorMessage.setVisibility(View.VISIBLE);
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.i("error", "error");
                lineChart.setVisibility(View.INVISIBLE);
                errorMessage.setVisibility(View.VISIBLE);
            }
        });

        queue.add(stringRequest);
    }

    // display current date
    public void setCurrentDateOnView() {

        tvDisplayDate = (TextView) findViewById(R.id.tvDate);
        dpResult = (DatePicker) findViewById(R.id.dpResult);

        final Calendar c = Calendar.getInstance();
        year = c.get(Calendar.YEAR);
        month = c.get(Calendar.MONTH);
        day = c.get(Calendar.DAY_OF_MONTH);

        // set current date into textview
        tvDisplayDate.setText(new StringBuilder()
                // Month is 0 based, just add 1
                .append(year).append("-").append(month + 1).append("-")
                .append(day).append(" "));

        // set current date into datepicker
        dpResult.init(year, month, day, null);

        newYear = year;
        newMonth = month + 1;
        newDay = day;
        nextDay = newDay + 1;
        fromDate = newYear + "-" + newMonth + "-" + newDay;
        Log.i("from: ", fromDate);
        toDate = newYear + "-" + newMonth + "-" + nextDay;
        Log.i("to: ", toDate);

    }

    public void addListenerOnButton() {
        btnChangeDate = (AppCompatButton) findViewById(R.id.tvDate);
        btnChangeDate.setOnClickListener(new View.OnClickListener() {

            @SuppressWarnings("deprecation")
            @Override
            public void onClick(View v) {
                showDialog(DATE_DIALOG_ID);
            }
        });

    }

    @SuppressWarnings("deprecation")
    @Override
    protected Dialog onCreateDialog(int id) {
        switch (id) {
            case DATE_DIALOG_ID:
                // set date picker as current date
                return new DatePickerDialog(this, datePickerListener,
                        year, month, day);
        }
        return null;
    }

    private DatePickerDialog.OnDateSetListener datePickerListener
            = new DatePickerDialog.OnDateSetListener() {

        // when dialog box is closed, below method will be called.
        public void onDateSet(DatePicker view, int selectedYear,
                              int selectedMonth, int selectedDay) {
            year = selectedYear;
            month = selectedMonth;
            day = selectedDay;

            // set selected date into textview
            tvDisplayDate.setText(new StringBuilder()
                    // Month is 0 based, just add 1
                    .append(year).append("-").append(month + 1).append("-")
                    .append(day).append(" "));

            // set selected date into datepicker also
            dpResult.init(year, month, day, null);
            newYear = year;
            newMonth = month + 1;
            newDay = day;
            nextDay = newDay + 1;
            fromDate = newYear + "-" + newMonth + "-" + newDay;
            Log.i("from: ", fromDate);
            toDate = newYear + "-" + newMonth + "-" + nextDay;
            Log.i("to: ", toDate);

        }
    };

}
