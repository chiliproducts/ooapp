package hu.uniobuda.nik.chili.ooapp;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;


/**
 * Created by Sali on 2016.03.18..
 */
public class ChartsMenuActivity extends AppCompatActivity

{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_charts_menu);

        Button btn_SumCharts = (Button) findViewById(R.id.btn_all_charts);
        btn_SumCharts.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(ChartsMenuActivity.this, SumChartsMenuActivity.class));
            }
        });

        Button btn_Periodic_Charts = (Button) findViewById(R.id.btn_periodic_charts);
        btn_Periodic_Charts.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(ChartsMenuActivity.this, IntervalChartsActivity.class));
            }
        });

        Button btn_Daily_Charts = (Button) findViewById(R.id.btn_daily_chart);
        btn_Daily_Charts.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(ChartsMenuActivity.this, DailyChartActivity.class));
            }
        });
    }
}





