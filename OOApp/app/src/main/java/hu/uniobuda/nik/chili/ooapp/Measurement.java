package hu.uniobuda.nik.chili.ooapp;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Balázs on 2016.03.29..
 */
public class Measurement {

    // JSON Node names

    private static final String TAG_ID = "id";
    private static final String TAG_SENSOR = "sensor";
    private static final String TAG_VALUE = "value";
    private static final String TAG_MEASURED = "measured";

    private Integer id;
    private String sensor;
    private Float value;
    //TODO:
    private String measured;

    public Measurement(JSONObject measurement) {
        try {
            id = measurement.getInt(TAG_ID);
            sensor = measurement.getString(TAG_SENSOR);
            value = ((float) measurement.getDouble(TAG_VALUE));
            measured = measurement.getString(TAG_MEASURED);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public Measurement() {

    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getSensor() {
        return sensor;
    }

    public void setSensor(String sensor) {
        this.sensor = sensor;
    }

    public Float getValue() {
        return value;
    }

    public void setValue(Float value) {
        this.value = value;
    }

    public String getMeasured() {
        return measured;
    }

    public void setMeasured(String measured) {
        this.measured = measured;
    }

    public void setContent(Measurement mes){
        this.id = mes.getId();
        this.value = mes.getValue();
        this.sensor = mes.getSensor();
        this.measured = mes.getMeasured();

    }
}
