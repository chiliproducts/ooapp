<?php
	
	require_once __DIR__ . '/db_config.php';

    $sensor = $_REQUEST["sensor"];

	$con = mysqli_connect(DB_SERVER, DB_USER, DB_PASSWORD, DB_DATABASE);

	if (!$con) {
		die("Error in connection: " .mysql_error());
	}
	else{

		$query = "SELECT * FROM measurements WHERE sensor = '$sensor' ORDER BY measured DESC LIMIT 1";
		
  		$res = mysqli_query($con,$query); 

		if ($res) {
			
			if (mysqli_num_rows($res) > 0){

	 			$result = mysqli_fetch_array($res,MYSQLI_BOTH);

	 			$measurement["id"] = $result["id"];
	            $measurement["sensor"] = $result["sensor"];
	            $measurement["value"] = $result["value"];
	            $measurement["measured"] = $result["measured"];
	 			
	            /*$measurements = array();
	            $measurements["id"] = $result["id"];
	            $measurements["sensor_id"] = $result["sensor_id"];
	            $measurements["value"] = $result["value"];
	            $measurements["measured"] = $result["measured"];*/
	        
	            $response["success"] = 1;
	            $response["measurement"] = $measurement;
	 
	            /*array_push($response["measurements"], $measurements);*/
	 
	          
	            echo json_encode($response);
			}
			else{
		
	            $response["success"] = 0;
	            $response["message"] = "No measurement found";
	 
	            echo json_encode($response);
			}
		}
		else{

			$response["success"] = 0;
            $response["message"] = "Missing parameter";

            echo json_encode($response);
		}

	}
	mysqli_close($con);
?>	