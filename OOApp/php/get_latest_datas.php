<?php
	
	require_once __DIR__ . '/db_config.php';

	$con = mysqli_connect(DB_SERVER, DB_USER, DB_PASSWORD, DB_DATABASE);

	if (!$con) {
		die("Error in connection: " .mysql_error());
	}
	else{

        $query = "SELECT f.id, f.sensor, f.value, f.measured FROM (SELECT sensor, MAX(measured) as meas FROM measurements group by sensor) as x inner join `measurements` as f on f.sensor = x.sensor and f.measured=x.meas";
		
  		$res = mysqli_query($con,$query); 

		if ($res) {
			
			if (mysqli_num_rows($res) > 0){

				$response["success"] = 1;
				$response["measurements"] = array();

	 			while ($result = mysqli_fetch_array($res,MYSQLI_BOTH)){

	 				$measurement["id"] = $result["id"];
	            	$measurement["sensor"] = $result["sensor"];
	            	$measurement["value"] = $result["value"];
	           	 	$measurement["measured"] = $result["measured"];

	           	 	$mes["measurement"] = $measurement;

	           	 	array_push($response["measurements"], $mes);
	 			}
                               
	            echo json_encode($response);
			}
			else{
		
	            $response["success"] = 0;
	            $response["message"] = "No measurement found";
	 
	            echo json_encode($response);
			}
		}
		else{

			$response["success"] = 0;
            $response["message"] = "Missing parameter";

            echo json_encode($response);
		}

	}
	mysqli_close($con);
?>	